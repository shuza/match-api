package server

import (
	"errors"
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	mcDepositSvc "match-api/internal/app/deposit/mocks"
	"match-api/internal/app/model"
	mcProductSvc "match-api/internal/app/product/mocks"
	mcUserSvc "match-api/internal/app/user/mocks"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func Test_UserSignup(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	testCases := []struct {
		desc          string
		payload       string
		mockSvc       func() *mcUserSvc.MockService
		expStatusCode int
		expResponse   string
	}{
		{
			desc:    "should return success",
			payload: `{ "username": "abcd", "password": "1234", "user_role": "SELLER" }`,
			mockSvc: func() *mcUserSvc.MockService {
				s := mcUserSvc.NewMockService(ctrl)
				s.EXPECT().CreateUser(gomock.Any(), gomock.Any()).Return(nil)
				return s
			},
			expStatusCode: http.StatusCreated,
			expResponse:   `{"success":true,"errors":null,"data":"successful"}`,
		},
		{
			desc:    "should return bad request",
			payload: `----`,
			mockSvc: func() *mcUserSvc.MockService {
				return mcUserSvc.NewMockService(ctrl)
			},
			expStatusCode: http.StatusUnprocessableEntity,
			expResponse:   `{"success":false,"errors":[{"code":"INVALID","message":"invalid character '-' in numeric literal","message_title":"bad request","severity":"error"}],"data":null}`,
		},
		{
			desc:    "should return invalid error",
			payload: `{ "username": ""}`,
			mockSvc: func() *mcUserSvc.MockService {
				s := mcUserSvc.NewMockService(ctrl)
				s.EXPECT().CreateUser(gomock.Any(), gomock.Any()).Return(fmt.Errorf("invliad username or password :%w", model.ErrInvalid))
				return s
			},
			expStatusCode: http.StatusBadRequest,
			expResponse:   `{"success":false,"errors":[{"code":"INVALID","message":"invliad username or password :invalid","message_title":"invalid request","severity":"error"}],"data":null}`,
		},
		{
			desc:    "should return server error",
			payload: `{ "username": ""}`,
			mockSvc: func() *mcUserSvc.MockService {
				s := mcUserSvc.NewMockService(ctrl)
				s.EXPECT().CreateUser(gomock.Any(), gomock.Any()).Return(errors.New("error"))
				return s
			},
			expStatusCode: http.StatusInternalServerError,
			expResponse:   `{"success":false,"errors":[{"code":"SERVER_ERROR","message":"error","message_title":"server error","severity":"error"}],"data":null}`,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			s := NewServer(":8080", tc.mockSvc(), nil, nil)
			w := httptest.NewRecorder()
			body := strings.NewReader(tc.payload)
			r := httptest.NewRequest(http.MethodPost, "/api/v1/signup", body)

			router := mux.NewRouter()
			router.Methods(http.MethodPost).Path("/api/v1/signup").HandlerFunc(s.userSignup)
			router.ServeHTTP(w, r)
			assert.Equal(t, tc.expStatusCode, w.Code)
			assert.Equal(t, tc.expResponse, w.Body.String())
		})
	}
}

func Test_Login(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	testCases := []struct {
		desc          string
		payload       string
		mockSvc       func() *mcUserSvc.MockService
		expStatusCode int
	}{
		{
			desc:    "should success",
			payload: `{"username": "asdf","password": "1234"}`,
			mockSvc: func() *mcUserSvc.MockService {
				r := mcUserSvc.NewMockService(ctrl)
				r.EXPECT().MatchUsernameAndPassword(gomock.Any(), "asdf", "1234").Return(model.User{ID: 1}, nil)
				r.EXPECT().CreateAuthToken(gomock.Any(), 1).Return("token", nil)
				return r
			},
			expStatusCode: http.StatusOK,
		},
		{
			desc:    "should return bad request",
			payload: `----`,
			mockSvc: func() *mcUserSvc.MockService {
				return mcUserSvc.NewMockService(ctrl)
			},
			expStatusCode: http.StatusUnprocessableEntity,
		},
		{
			desc:    "user not found",
			payload: `{"username": "asdf","password": "1234"}`,
			mockSvc: func() *mcUserSvc.MockService {
				r := mcUserSvc.NewMockService(ctrl)
				r.EXPECT().MatchUsernameAndPassword(gomock.Any(), "asdf", "1234").Return(model.User{}, errors.New("not found"))
				return r
			},
			expStatusCode: http.StatusNotFound,
		},
		{
			desc:    "should success",
			payload: `{"username": "asdf","password": "1234"}`,
			mockSvc: func() *mcUserSvc.MockService {
				r := mcUserSvc.NewMockService(ctrl)
				r.EXPECT().MatchUsernameAndPassword(gomock.Any(), "asdf", "1234").Return(model.User{ID: 1}, nil)
				r.EXPECT().CreateAuthToken(gomock.Any(), 1).Return("", errors.New("err"))
				return r
			},
			expStatusCode: http.StatusInternalServerError,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			s := NewServer(":8080", tc.mockSvc(), nil, nil)
			w := httptest.NewRecorder()
			body := strings.NewReader(tc.payload)
			r := httptest.NewRequest(http.MethodPost, "/api/v1/login", body)

			router := mux.NewRouter()
			router.Methods(http.MethodPost).Path("/api/v1/login").HandlerFunc(s.login)
			router.ServeHTTP(w, r)
			assert.Equal(t, tc.expStatusCode, w.Code)
		})
	}
}

func Test_CreateDeposit(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	testCases := []struct {
		desc           string
		payload        string
		mockUserSvc    func() *mcUserSvc.MockService
		mockDepositSvc func() *mcDepositSvc.MockService
		expStatusCode  int
	}{
		{
			desc:    "should success",
			payload: `{"coin": 5,"quantity": 2}`,
			mockUserSvc: func() *mcUserSvc.MockService {
				s := mcUserSvc.NewMockService(ctrl)
				s.EXPECT().GetUserByToken(gomock.Any(), gomock.Any()).Return(model.User{ID: 1, UserRole: model.USER_ROLE_SELLER}, nil)
				return s
			},
			mockDepositSvc: func() *mcDepositSvc.MockService {
				s := mcDepositSvc.NewMockService(ctrl)
				s.EXPECT().Deposit(gomock.Any(), 1, 5, 2).Return(nil)
				return s
			},
			expStatusCode: http.StatusOK,
		},
		{
			desc:    "bad request",
			payload: `---`,
			mockUserSvc: func() *mcUserSvc.MockService {
				s := mcUserSvc.NewMockService(ctrl)
				s.EXPECT().GetUserByToken(gomock.Any(), gomock.Any()).Return(model.User{ID: 1, UserRole: model.USER_ROLE_SELLER}, nil)
				return s
			},
			mockDepositSvc: func() *mcDepositSvc.MockService {
				return mcDepositSvc.NewMockService(ctrl)
			},
			expStatusCode: http.StatusUnprocessableEntity,
		},
		{
			desc:    "internal server error",
			payload: `{"coin": 5,"quantity": 2}`,
			mockUserSvc: func() *mcUserSvc.MockService {
				s := mcUserSvc.NewMockService(ctrl)
				s.EXPECT().GetUserByToken(gomock.Any(), gomock.Any()).Return(model.User{ID: 1, UserRole: model.USER_ROLE_SELLER}, nil)
				return s
			},
			mockDepositSvc: func() *mcDepositSvc.MockService {
				s := mcDepositSvc.NewMockService(ctrl)
				s.EXPECT().Deposit(gomock.Any(), 1, 5, 2).Return(errors.New("err"))
				return s
			},
			expStatusCode: http.StatusInternalServerError,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			s := NewServer(":8080", tc.mockUserSvc(), nil, tc.mockDepositSvc())
			w := httptest.NewRecorder()
			body := strings.NewReader(tc.payload)
			r := httptest.NewRequest(http.MethodPost, "/api/v1/deposit", body)

			router := mux.NewRouter()
			router.Methods(http.MethodPost).Path("/api/v1/deposit").HandlerFunc(s.userAuth(s.CreateDeposit))
			router.ServeHTTP(w, r)
			assert.Equal(t, tc.expStatusCode, w.Code)
		})
	}
}

func Test_CreateProduct(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	testCases := []struct {
		desc           string
		payload        string
		mockUserSvc    func() *mcUserSvc.MockService
		mockProductSvc func() *mcProductSvc.MockService
		expStatusCode  int
	}{
		{
			desc:    "should success",
			payload: `{"name": "product-1","price": 15}`,
			mockUserSvc: func() *mcUserSvc.MockService {
				s := mcUserSvc.NewMockService(ctrl)
				s.EXPECT().GetUserByToken(gomock.Any(), gomock.Any()).Return(model.User{ID: 1, UserRole: model.USER_ROLE_SELLER}, nil)
				return s
			},
			mockProductSvc: func() *mcProductSvc.MockService {
				s := mcProductSvc.NewMockService(ctrl)
				s.EXPECT().CreateProduct(gomock.Any(), model.USER_ROLE_SELLER, gomock.Any()).Return(nil)
				return s
			},
			expStatusCode: http.StatusCreated,
		},
		{
			desc:    "bad request",
			payload: `---`,
			mockUserSvc: func() *mcUserSvc.MockService {
				s := mcUserSvc.NewMockService(ctrl)
				s.EXPECT().GetUserByToken(gomock.Any(), gomock.Any()).Return(model.User{ID: 1, UserRole: model.USER_ROLE_SELLER}, nil)
				return s
			},
			mockProductSvc: func() *mcProductSvc.MockService {
				return mcProductSvc.NewMockService(ctrl)
			},
			expStatusCode: http.StatusUnprocessableEntity,
		},
		{
			desc:    "invalid request",
			payload: `{"name": "product-1","price": 15}`,
			mockUserSvc: func() *mcUserSvc.MockService {
				s := mcUserSvc.NewMockService(ctrl)
				s.EXPECT().GetUserByToken(gomock.Any(), gomock.Any()).Return(model.User{ID: 1, UserRole: model.USER_ROLE_SELLER}, nil)
				return s
			},
			mockProductSvc: func() *mcProductSvc.MockService {
				s := mcProductSvc.NewMockService(ctrl)
				s.EXPECT().CreateProduct(gomock.Any(), model.USER_ROLE_SELLER, gomock.Any()).Return(model.ErrInvalid)
				return s
			},
			expStatusCode: http.StatusUnprocessableEntity,
		},
		{
			desc:    "internal server error",
			payload: `{"name": "product-1","price": 15}`,
			mockUserSvc: func() *mcUserSvc.MockService {
				s := mcUserSvc.NewMockService(ctrl)
				s.EXPECT().GetUserByToken(gomock.Any(), gomock.Any()).Return(model.User{ID: 1, UserRole: model.USER_ROLE_SELLER}, nil)
				return s
			},
			mockProductSvc: func() *mcProductSvc.MockService {
				s := mcProductSvc.NewMockService(ctrl)
				s.EXPECT().CreateProduct(gomock.Any(), model.USER_ROLE_SELLER, gomock.Any()).Return(errors.New("err"))
				return s
			},
			expStatusCode: http.StatusInternalServerError,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			s := NewServer(":8080", tc.mockUserSvc(), tc.mockProductSvc(), nil)
			w := httptest.NewRecorder()
			body := strings.NewReader(tc.payload)
			r := httptest.NewRequest(http.MethodPost, "/api/v1/product", body)

			router := mux.NewRouter()
			router.Methods(http.MethodPost).Path("/api/v1/product").HandlerFunc(s.userAuth(s.CreateProduct))
			router.ServeHTTP(w, r)
			assert.Equal(t, tc.expStatusCode, w.Code)
		})
	}
}
