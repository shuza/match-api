package server

import (
	"context"
	"match-api/internal/app/model"
	"net/http"
)

func (s *server) userAuth(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, err := s.userService.GetUserByToken(r.Context(), r.Header.Get("Authorization"))
		if err != nil {
			ErrUnauthorizedResponse(w, "AUTH_ERROR", err)
			return
		}
		ctx := context.WithValue(r.Context(), model.UserIdKey, user.ID)
		ctx = context.WithValue(ctx, model.UserTypeKey, user.UserRole)
		next(w, r.WithContext(ctx))
	}
}
