package server

import (
	"errors"
	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"match-api/internal/app/model"
	userMock "match-api/internal/app/user/mocks"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestMiddleware_UserAuth(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	t.Run("should authorize", func(t *testing.T) {
		userRepo := userMock.NewMockService(ctrl)
		userRepo.EXPECT().GetUserByToken(gomock.Any(), gomock.Any()).Return(model.User{ID: 1, UserRole: model.USER_ROLE_SELLER}, nil)

		s := NewServer("", userRepo, nil, nil)
		routs := mux.NewRouter()
		routs.Methods(http.MethodGet).Path("/ping").HandlerFunc(s.userAuth(s.pingHandler))
		w := httptest.NewRecorder()
		r, _ := http.NewRequest(http.MethodGet, "/ping", nil)

		routs.ServeHTTP(w, r)
		assert.Equal(t, http.StatusOK, w.Code)
	})

	t.Run("should not authorize", func(t *testing.T) {
		userRepo := userMock.NewMockService(ctrl)
		userRepo.EXPECT().GetUserByToken(gomock.Any(), gomock.Any()).Return(model.User{}, errors.New("err"))

		s := NewServer("", userRepo, nil, nil)
		routs := mux.NewRouter()
		routs.Methods(http.MethodGet).Path("/ping").HandlerFunc(s.userAuth(s.pingHandler))
		w := httptest.NewRecorder()
		r, _ := http.NewRequest(http.MethodGet, "/ping", nil)

		routs.ServeHTTP(w, r)
		assert.Equal(t, http.StatusUnauthorized, w.Code)
	})
}
