package server

import (
	"context"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"match-api/internal/app/deposit"
	"match-api/internal/app/product"
	"match-api/internal/app/user"
	"net/http"
)

type server struct {
	listenAddress  string
	http           *http.Server
	userService    user.Service
	productService product.Service
	depositService deposit.Service
}

func NewServer(port string, userSvc user.Service, productSvc product.Service, depositSvc deposit.Service) *server {
	s := &server{
		listenAddress:  port,
		userService:    userSvc,
		productService: productSvc,
		depositService: depositSvc,
	}
	s.http = &http.Server{
		Addr:    port,
		Handler: s.route(),
	}
	return s
}

func (s *server) route() *mux.Router {
	r := mux.NewRouter()
	r.Methods(http.MethodGet).Path("/ping").HandlerFunc(s.pingHandler)
	apiRoute := r.PathPrefix("/api/v1").Subrouter()
	apiRoute.Methods(http.MethodPost).Path("/signup").HandlerFunc(s.userSignup)
	apiRoute.Methods(http.MethodPost).Path("/login").HandlerFunc(s.login)
	apiRoute.Methods(http.MethodPost).Path("/products").HandlerFunc(s.userAuth(s.CreateProduct))
	apiRoute.Methods(http.MethodGet).Path("/products").HandlerFunc(s.userAuth(s.GetProductList))
	apiRoute.Methods(http.MethodPost).Path("/deposit").HandlerFunc(s.userAuth(s.CreateDeposit))
	apiRoute.Methods(http.MethodPost).Path("/buy").HandlerFunc(s.userAuth(s.BuyProduct))
	return r
}

func (s *server) Run() error {
	log.Info().Msgf("start listen server in %s", s.listenAddress)
	if err := s.http.ListenAndServe(); err != nil {
		if err != http.ErrServerClosed {
			log.Error().Err(err).Msg("unexpected error while running server")
			return s.Shutdown()
		}
	}
	return nil
}

func (s *server) Shutdown() error {
	log.Info().Msg("shutting down server")
	if err := s.http.Shutdown(context.Background()); err != nil {
		if err != http.ErrServerClosed {
			return err
		}
	}
	return nil
}

func (s *server) pingHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(`{"success": "ping"}`))
}
