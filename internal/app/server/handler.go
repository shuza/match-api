package server

import (
	"encoding/json"
	"errors"
	"github.com/rs/zerolog/log"
	"match-api/internal/app/model"
	"net/http"
	"strconv"
)

func (s *server) userSignup(w http.ResponseWriter, r *http.Request) {
	var data model.User
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		ErrUnprocessableEntityResponse(w, "bad request", err)
		return
	}
	if err := s.userService.CreateUser(r.Context(), data); err != nil {
		if errors.Is(err, model.ErrInvalid) {
			ErrInvalidEntityResponse(w, "invalid request", err)
			return
		}
		log.Error().Err(err).Msg("[handler.userSignup] failed to create user")
		ErrInternalServerResponse(w, "server error", err)
		return
	}
	SuccessResponse(w, http.StatusCreated, "successful")
}

func (s *server) login(w http.ResponseWriter, r *http.Request) {
	var data model.User
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		ErrUnprocessableEntityResponse(w, "bad request", err)
		return
	}
	userData, err := s.userService.MatchUsernameAndPassword(r.Context(), data.Username, data.Password)
	if err != nil {
		ErrNotFoundResponse(w, "user not found", err)
		return
	}
	token, err := s.userService.CreateAuthToken(r.Context(), userData.ID)
	if err != nil {
		ErrInternalServerResponse(w, "failed to create token", err)
		return
	}
	SuccessResponse(w, http.StatusOK, token)
}

func (s *server) CreateDeposit(w http.ResponseWriter, r *http.Request) {
	var data model.Deposit
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		ErrUnprocessableEntityResponse(w, "bad request", err)
		return
	}
	userID, ok := r.Context().Value(model.UserIdKey).(int)
	if !ok {
		log.Error().Msg("userID not found in context")
		ErrInternalServerResponse(w, "SERVER_ERROR", errors.New("missing userID in context"))
		return
	}
	data.UserID = userID
	if err := s.depositService.Deposit(r.Context(), data.UserID, data.Value, data.Quantity); err != nil {
		log.Error().Err(err).Msg("failed to deposit coin")
		ErrInternalServerResponse(w, "SERVER_ERROR", err)
		return
	}
	SuccessResponse(w, http.StatusOK, "Successful")
}

func (s *server) CreateProduct(w http.ResponseWriter, r *http.Request) {
	var data model.Product
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		ErrUnprocessableEntityResponse(w, "bad request", err)
		return
	}
	userID, ok := r.Context().Value(model.UserIdKey).(int)
	if !ok {
		log.Error().Msg("userID not found in context")
		ErrInternalServerResponse(w, "SERVER_ERROR", errors.New("missing userID in context"))
		return
	}
	userType, ok := r.Context().Value(model.UserTypeKey).(string)
	if !ok {
		log.Error().Msg("userType not found in context")
		ErrInternalServerResponse(w, "SERVER_ERROR", errors.New("missing userType in context"))
		return
	}
	data.OwnerId = userID
	if err := s.productService.CreateProduct(r.Context(), userType, data); err != nil {
		if errors.Is(err, model.ErrInvalid) {
			ErrUnprocessableEntityResponse(w, "INVALID", err)
			return
		}
		log.Error().Err(err).Msg("failed to create product")
		ErrInternalServerResponse(w, "SERVER_ERROR", err)
		return
	}

	SuccessResponse(w, http.StatusCreated, "product created")
}

func (s *server) GetProductList(w http.ResponseWriter, r *http.Request) {
	start, err := strconv.Atoi(r.URL.Query().Get("start"))
	if err != nil {
		ErrUnprocessableEntityResponse(w, "INVALID start", err)
		return
	}
	limit, err := strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		ErrUnprocessableEntityResponse(w, "INVALID limit", err)
		return
	}
	products, err := s.productService.GetProductList(r.Context(), start, limit)
	if err != nil {
		log.Error().Err(err).Msg("failed to fetch product list")
		ErrInternalServerResponse(w, "SERVER_ERROR", err)
		return
	}
	SuccessResponse(w, http.StatusOK, products)

}

func (s *server) BuyProduct(w http.ResponseWriter, r *http.Request) {
	var data model.Buy
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		ErrUnprocessableEntityResponse(w, "bad request", err)
		return
	}

	userID, ok := r.Context().Value(model.UserIdKey).(int)
	if !ok {
		log.Error().Msg("userID not found in context")
		ErrInternalServerResponse(w, "SERVER_ERROR", errors.New("missing userID in context"))
		return
	}
	data.UserID = userID
	userCoin, err := s.userService.GetUserCoin(r.Context(), userID)
	if err != nil {
		log.Error().Err(err).Msgf("failed get coin for user_id: %d", userID)
		ErrInternalServerResponse(w, "SERVER_ERROR", err)
		return
	}
	if err = s.productService.BuyProduct(r.Context(), userCoin, data); err != nil {
		log.Error().Err(err).Msg("failed to buy product")
		ErrInternalServerResponse(w, "SERVER_ERROR", err)
		return
	}
	userCoin, err = s.userService.GetUserCoin(r.Context(), userID)
	if err != nil {
		log.Error().Err(err).Msgf("failed get coin for user_id: %d", userID)
		ErrInternalServerResponse(w, "SERVER_ERROR", err)
		return
	}
	SuccessResponse(w, http.StatusOK, userCoin)
}
