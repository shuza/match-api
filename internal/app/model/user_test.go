package model

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestUser_ValidateCreateRequest(t *testing.T) {
	t.Run("check empty username", func(t *testing.T) {
		data := User{}
		err := data.ValidateCreateRequest()
		assert.True(t, errors.Is(err, ErrInvalid))
	})

	t.Run("check empty password", func(t *testing.T) {
		data := User{
			Username: "a",
		}
		err := data.ValidateCreateRequest()
		assert.True(t, errors.Is(err, ErrInvalid))
	})

	t.Run("check empty user role", func(t *testing.T) {
		data := User{
			Username: "a",
			Password: "a",
		}
		err := data.ValidateCreateRequest()
		assert.True(t, errors.Is(err, ErrInvalid))
	})

	t.Run("check invalid user role", func(t *testing.T) {
		data := User{
			Username: "a",
			Password: "a",
			UserRole: "invalid",
		}
		err := data.ValidateCreateRequest()
		assert.True(t, errors.Is(err, ErrInvalid))
	})

	t.Run("should success", func(t *testing.T) {
		data := User{
			Username: "a",
			Password: "a",
			UserRole: USER_ROLE_SELLER,
		}
		err := data.ValidateCreateRequest()
		assert.Nil(t, err)
	})
}
