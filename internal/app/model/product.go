package model

import "fmt"

type Product struct {
	ID      int    `json:"id" db:"id"`
	Name    string `json:"name" db:"name"`
	Price   int    `json:"price" db:"price"`
	OwnerId int    `json:"owner_id,omitempty" db:"owner_id"`
}

func (p *Product) ValidateCreateRequest() error {
	if p.Name == "" || p.Price <= 0 || p.OwnerId < 1 {
		return fmt.Errorf("invalid name or price :%w", ErrInvalid)
	}
	return nil
}
