package model

type Buy struct {
	ID        int    `json:"id"`
	UserID    int    `json:"user_id"`
	ProductID int    `json:"product_id"`
	Quantity  int    `json:"quantity"`
	Price     int    `json:"price"`
	Coins     []Coin `json:"coins"`
}
