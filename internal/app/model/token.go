package model

import "time"

type UserToken struct {
	ID     int       `json:"id" db:"id"`
	UserID int       `json:"user_id" db:"user_id"`
	Token  string    `json:"token" db:"token"`
	Expire time.Time `json:"expire" db:"expire"`
}
