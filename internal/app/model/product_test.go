package model

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestProduct_ValidateCreateRequest(t *testing.T) {
	t.Run("check empty name", func(t *testing.T) {
		p := Product{}
		err := p.ValidateCreateRequest()
		assert.True(t, errors.Is(err, ErrInvalid))
	})
	t.Run("check empty price", func(t *testing.T) {
		p := Product{
			Name: "a",
		}
		err := p.ValidateCreateRequest()
		assert.True(t, errors.Is(err, ErrInvalid))
	})
	t.Run("should return no error", func(t *testing.T) {
		p := Product{
			Name:    "a",
			Price:   2,
			OwnerId: 1,
		}
		err := p.ValidateCreateRequest()
		assert.Nil(t, err)
	})
}
