package model

type Deposit struct {
	Coin
	UserID int `json:"user_id" db:"user_id"`
	Amount int `json:"amount" db:"amount"`
}
