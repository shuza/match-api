package model

type Coin struct {
	Value    int `json:"coin" db:"coin"`
	Quantity int `json:"quantity" db:"quantity"`
}
