package model

import "fmt"

const (
	USER_ROLE_SELLER = "SELLER"
	USER_ROLE_BUYER  = "BUYER"
)

type User struct {
	ID       int    `json:"id" db:"id"`
	Username string `json:"username" db:"username"`
	Password string `json:"password" db:"password"`
	UserRole string `json:"user_role" db:"user_role"`
}

func (u *User) ValidateCreateRequest() error {
	if u.Username == "" || u.Password == "" {
		return fmt.Errorf("invliad username or password :%w", ErrInvalid)
	}
	if u.UserRole != USER_ROLE_SELLER && u.UserRole != USER_ROLE_BUYER {
		return fmt.Errorf("invliad user role :%w", ErrInvalid)
	}
	return nil
}
