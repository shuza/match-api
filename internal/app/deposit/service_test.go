package deposit

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"match-api/internal/app/deposit/mocks"
	"testing"
)

func TestSvc_Deposit(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	t.Run("should success", func(t *testing.T) {
		r := mocks.NewMockrepository(ctrl)
		r.EXPECT().Insert(gomock.Any(), 1, 2, 3).Return(nil)
		s := NewService(r)
		err := s.Deposit(context.Background(), 1, 2, 3)
		assert.Nil(t, err)
	})

	t.Run("should return error", func(t *testing.T) {
		r := mocks.NewMockrepository(ctrl)
		r.EXPECT().Insert(gomock.Any(), 1, 2, 3).Return(errors.New("repo-err"))
		s := NewService(r)
		err := s.Deposit(context.Background(), 1, 2, 3)
		assert.NotNil(t, err)
	})
}
