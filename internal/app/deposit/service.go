package deposit

import "context"

//go:generate mockgen -source service.go -destination ./mocks/mock_service.go -package mocks

type Service interface {
	Deposit(ctx context.Context, userID, coin, quantity int) error
}

type svc struct {
	repo repository
}

func NewService(repo repository) *svc {
	return &svc{
		repo: repo,
	}
}

func (s *svc) Deposit(ctx context.Context, userID, coin, quantity int) error {
	return s.repo.Insert(ctx, userID, coin, quantity)
}
