package deposit

import (
	"context"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog/log"
)

//go:generate mockgen -source repository.go -destination ./mocks/mock_repository.go -package mocks

type repository interface {
	Insert(ctx context.Context, userID int, coin, quantity int) error
}

const (
	insertCoinQuery     = "INSERT INTO deposit (user_id, coin, quantity, amount) VALUES ($1, $2, $3, $4)"
	upsertUserCoinQuery = "INSERT INTO user_coin (user_id, coin, quantity) VALUES ($1, $2, $3) ON CONFLICT (user_id, coin) DO UPDATE SET quantity = user_coin.quantity + $3;"
)

type repo struct {
	db *sqlx.DB
}

func NewRepository(db *sqlx.DB) *repo {
	return &repo{
		db: db,
	}
}

func (r *repo) Insert(ctx context.Context, userID, coin, quantity int) error {
	tx, err := r.db.Beginx()
	if err != nil {
		log.Error().Err(err).Msg("[deposit.Insert] failed to begin transaction")
		return err
	}
	if _, err := tx.ExecContext(ctx, insertCoinQuery, userID, coin, quantity, coin*quantity); err != nil {
		log.Error().Err(err).Msgf("[deposit.Insert] failed to deposit user_id: %d coin: %d quantity: %d", userID, coin, quantity)
		tx.Rollback()
		return err
	}

	if _, err := tx.ExecContext(ctx, upsertUserCoinQuery, userID, coin, quantity); err != nil {
		log.Error().Err(err).Msgf("[deposit.Insert] failed update user_coin user_id: %d coin: %d quantity: %d", userID, coin, quantity)
		tx.Rollback()
		return err
	}

	if err := tx.Commit(); err != nil {
		log.Error().Err(err).Msg("[deposit.Insert] failed to commit transaction")
		tx.Rollback()
		return err
	}
	return nil
}
