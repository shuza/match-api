package deposit

import (
	"context"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRepo_Insert(t *testing.T) {
	t.Run("should return success", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectBegin()
		m.ExpectExec("INSERT INTO deposit (.+) VALUES (.+)").
			WithArgs(1, 10, 2, 20).
			WillReturnResult(sqlmock.NewResult(1, 1))

		m.ExpectExec("INSERT INTO user_coin").
			WithArgs(1, 10, 2).
			WillReturnResult(sqlmock.NewResult(1, 1))
		m.ExpectCommit()

		r := NewRepository(sqlxDB)
		err := r.Insert(context.Background(), 1, 10, 2)
		assert.Nil(t, err)
	})

	t.Run("failed to begin", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectBegin().WillReturnError(errors.New("tx-err"))
		r := NewRepository(sqlxDB)
		err := r.Insert(context.Background(), 1, 10, 2)
		assert.NotNil(t, err)
	})

	t.Run("failed to deposit", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectBegin()
		m.ExpectExec("INSERT INTO deposit (.+) VALUES (.+)").
			WithArgs(1, 10, 2, 20).
			WillReturnError(errors.New("err"))
		m.ExpectRollback()

		r := NewRepository(sqlxDB)
		err := r.Insert(context.Background(), 1, 10, 2)
		assert.NotNil(t, err)
	})

	t.Run("failed to update user coin", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectBegin()
		m.ExpectExec("INSERT INTO deposit (.+) VALUES (.+)").
			WithArgs(1, 10, 2, 20).
			WillReturnResult(sqlmock.NewResult(1, 1))

		m.ExpectExec("INSERT INTO user_coin").
			WithArgs(1, 10, 2).
			WillReturnError(errors.New("err"))
		m.ExpectRollback()

		r := NewRepository(sqlxDB)
		err := r.Insert(context.Background(), 1, 10, 2)
		assert.NotNil(t, err)
	})

	t.Run("failed to commit", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectBegin()
		m.ExpectExec("INSERT INTO deposit (.+) VALUES (.+)").
			WithArgs(1, 10, 2, 20).
			WillReturnResult(sqlmock.NewResult(1, 1))

		m.ExpectExec("INSERT INTO user_coin").
			WithArgs(1, 10, 2).
			WillReturnResult(sqlmock.NewResult(1, 1))
		m.ExpectCommit().WillReturnError(errors.New("err"))
		m.ExpectRollback()

		r := NewRepository(sqlxDB)
		err := r.Insert(context.Background(), 1, 10, 2)
		assert.NotNil(t, err)
	})
}
