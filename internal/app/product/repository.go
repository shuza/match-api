package product

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	"github.com/rs/zerolog/log"
	"match-api/internal/app/model"
)

//go:generate mockgen -source repository.go -destination ./mocks/mock_repository.go -package mocks

type repository interface {
	Insert(ctx context.Context, data model.Product) error
	GetProducts(ctx context.Context, start, limit int) ([]model.Product, error)
	ProductDetails(ctx context.Context, id int) (model.Product, error)
	Buy(ctx context.Context, data model.Buy) error
}

const (
	errUniqueViolation     = pq.ErrorCode("23505")
	insertProductQuery     = "INSERT INTO products (name, price, owner_id) VALUES (:name, :price, :owner_id)"
	getProductsQuery       = "SELECT id, name, price FROM products OFFSET $1 LIMIT $2"
	insertBuyQuery         = "INSERT INTO buy (user_id, product_id, quantity, price) VALUES ($1, $2, $3, $4) RETURNING id"
	insertTransactionQuery = "INSERT INTO transactions (user_id, buy_id, amount) VALUES ($1, $2, $3)"
	deductUserCoinQuery    = "UPDATE user_coin set quantity = quantity - $1 WHERE user_id = $2 AND coin = $3"
	productDetailsQuery    = "SELECT id, name, price, owner_id FROM products WHERE id = $1"
)

type repo struct {
	db *sqlx.DB
}

func NewRepository(db *sqlx.DB) *repo {
	return &repo{
		db: db,
	}
}

func (r *repo) Insert(ctx context.Context, data model.Product) error {
	stmt, err := r.db.PrepareNamedContext(ctx, insertProductQuery)
	if err != nil {
		log.Error().Err(err).Msg("[product.Insert] failed to prepare insert product query")
		return err
	}
	_, err = stmt.ExecContext(ctx, &data)
	if err != nil {
		if pqErr, ok := err.(*pq.Error); ok && pqErr.Code == errUniqueViolation {
			return fmt.Errorf("%v :%w", err, model.ErrInvalid)
		}
		log.Error().Err(err).Msg("[product.Insert] failed to execute query")
		return err
	}
	return nil
}

func (r *repo) ProductDetails(ctx context.Context, id int) (model.Product, error) {
	var data model.Product
	if err := r.db.GetContext(ctx, &data, productDetailsQuery, id); err != nil {
		log.Error().Err(err).Msgf("[product.ProductDetails] failed to fetch details for product_id: %d", id)
		return model.Product{}, err
	}
	return data, nil
}

func (r *repo) GetProducts(ctx context.Context, start, limit int) ([]model.Product, error) {
	var data []model.Product
	if err := r.db.SelectContext(ctx, &data, getProductsQuery, start, limit); err != nil {
		log.Error().Err(err).Msgf("[product.GetProducts] start: %d limit: %d failed to fetch data", start, limit)
		return nil, err
	}
	return data, nil
}

func (r *repo) Buy(ctx context.Context, data model.Buy) error {
	tx, err := r.db.Beginx()
	if err != nil {
		log.Error().Err(err).Msg("[product.Buy] failed to create transaction")
		return err
	}
	var buyID int
	if err = tx.GetContext(ctx, &buyID, insertBuyQuery, data.UserID, data.ProductID, data.Quantity, data.Price); err != nil {
		log.Error().Err(err).Msgf("[product.Buy] failed to buy product_id: %d user_id: %d quantity: %d  price:  %d", data.ProductID, data.UserID, data.Quantity, data.Price)
		tx.Rollback()
		return err
	}

	if _, err = tx.ExecContext(ctx, insertTransactionQuery, data.UserID, buyID, data.Price); err != nil {
		log.Error().Err(err).Msgf("[product.Buy] failed create transaction product_id: %d user_id: %d quantity: %d  price:  %d", data.ProductID, data.UserID, data.Quantity, data.Price)
		tx.Rollback()
		return err
	}

	for _, v := range data.Coins {
		if _, err = tx.ExecContext(ctx, deductUserCoinQuery, v.Quantity, data.UserID, v.Value); err != nil {
			log.Error().Err(err).Msgf("[product.Buy] failed to deduct user_coin user_id: %d coin: %d quantity: %d", data.UserID, v.Value, v.Quantity)
			tx.Rollback()
			return err
		}
	}

	if err = tx.Commit(); err != nil {
		log.Error().Err(err).Msg("[product.Buy] failed to commit transaction")
		tx.Rollback()
		return err
	}
	return nil
}
