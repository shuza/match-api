package product

import (
	"context"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"match-api/internal/app/model"
	"testing"
)

func TestRepo_Insert(t *testing.T) {
	t.Run("should success", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectPrepare("INSERT INTO products").
			ExpectExec().
			WithArgs("product-1", 10, 1).
			WillReturnResult(sqlmock.NewResult(1, 1))

		r := NewRepository(sqlxDB)
		err := r.Insert(context.Background(), model.Product{Name: "product-1", Price: 10, OwnerId: 1})
		assert.Nil(t, err)
	})

	t.Run("return prepare error", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectPrepare("INSERT INTO products").
			WillReturnError(errors.New("err"))

		r := NewRepository(sqlxDB)
		err := r.Insert(context.Background(), model.Product{Name: "product-1", Price: 10, OwnerId: 1})
		assert.NotNil(t, err)
	})

	t.Run("return unique key violation", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectPrepare("INSERT INTO products").
			ExpectExec().
			WithArgs("product-1", 10, 1).
			WillReturnError(&pq.Error{Code: "23505"})

		r := NewRepository(sqlxDB)
		err := r.Insert(context.Background(), model.Product{Name: "product-1", Price: 10, OwnerId: 1})
		assert.True(t, errors.Is(err, model.ErrInvalid))
	})

	t.Run("return db error", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectPrepare("INSERT INTO products").
			ExpectExec().
			WithArgs("product-1", 10, 1).
			WillReturnError(errors.New("db-err"))

		r := NewRepository(sqlxDB)
		err := r.Insert(context.Background(), model.Product{Name: "product-1", Price: 10, OwnerId: 1})
		assert.False(t, errors.Is(err, model.ErrInvalid))
	})
}

func TestRepo_ProductDetails(t *testing.T) {
	t.Run("should success", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		product := model.Product{
			ID:      1,
			Name:    "product-1",
			Price:   10,
			OwnerId: 1,
		}
		m.ExpectQuery("SELECT id, name, price, owner_id FROM products ").
			WithArgs(product.ID).
			WillReturnRows(sqlmock.NewRows([]string{"id", "name", "price", "owner_id"}).AddRow(product.ID, product.Name, product.Price, product.OwnerId))

		r := NewRepository(sqlxDB)
		data, err := r.ProductDetails(context.Background(), product.ID)
		assert.EqualValues(t, product, data)
		assert.Nil(t, err)
	})

	t.Run("return error", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectQuery("SELECT id, name, price, owner_id FROM products ").
			WithArgs(1).
			WillReturnError(errors.New("db-err"))

		r := NewRepository(sqlxDB)
		data, err := r.ProductDetails(context.Background(), 1)
		assert.EqualValues(t, model.Product{}, data)
		assert.NotNil(t, err)
	})
}

func TestRepo_GetProducts(t *testing.T) {
	t.Run("should success", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		products := []model.Product{
			{
				ID:    1,
				Name:  "product-1",
				Price: 10,
			},
		}

		m.ExpectQuery("SELECT id, name, price FROM products").
			WithArgs(0, 10).
			WillReturnRows(sqlmock.NewRows([]string{"id", "name", "price"}).AddRow(1, "product-1", 10))

		r := NewRepository(sqlxDB)
		data, err := r.GetProducts(context.Background(), 0, 10)
		assert.EqualValues(t, products, data)
		assert.Nil(t, err)
	})

	t.Run("return error", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectQuery("SELECT id, name, price FROM products").
			WithArgs(0, 10).
			WillReturnError(errors.New("db-err"))

		r := NewRepository(sqlxDB)
		data, err := r.GetProducts(context.Background(), 0, 10)
		assert.Nil(t, data)
		assert.NotNil(t, err)
	})
}

func TestRepo_Buy(t *testing.T) {
	buy := model.Buy{
		ID:        1,
		UserID:    1,
		ProductID: 1,
		Quantity:  1,
		Price:     10,
		Coins:     []model.Coin{{Value: 10, Quantity: 1}},
	}

	t.Run("should success", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectBegin()
		m.ExpectQuery("INSERT INTO buy (.+) VALUES (.+) RETURNING id").
			WithArgs(buy.UserID, buy.ProductID, buy.Quantity, buy.Price).
			WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(buy.ID))
		m.ExpectExec("INSERT INTO transactions (.+) VALUES (.+)").
			WithArgs(buy.UserID, buy.ID, buy.Price).
			WillReturnResult(sqlmock.NewResult(1, 1))
		m.ExpectExec("UPDATE user_coin").
			WithArgs(buy.Coins[0].Quantity, buy.UserID, buy.Coins[0].Value).
			WillReturnResult(sqlmock.NewResult(1, 1))
		m.ExpectCommit()

		r := NewRepository(sqlxDB)
		err := r.Buy(context.Background(), buy)
		assert.Nil(t, err)
	})

	t.Run("fail begin transaction", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectBegin().WillReturnError(errors.New("err"))
		r := NewRepository(sqlxDB)
		err := r.Buy(context.Background(), model.Buy{})
		assert.NotNil(t, err)
	})

	t.Run("fail to create buy", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectBegin()
		m.ExpectQuery("INSERT INTO buy (.+) VALUES (.+) RETURNING id").
			WithArgs(buy.UserID, buy.ProductID, buy.Quantity, buy.Price).
			WillReturnError(errors.New("err"))
		m.ExpectRollback()
		m.ExpectBegin().WillReturnError(errors.New("err"))

		r := NewRepository(sqlxDB)
		err := r.Buy(context.Background(), buy)
		assert.NotNil(t, err)
	})

	t.Run("fail to create transaction", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectBegin()
		m.ExpectQuery("INSERT INTO buy (.+) VALUES (.+) RETURNING id").
			WithArgs(buy.UserID, buy.ProductID, buy.Quantity, buy.Price).
			WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(buy.ID))
		m.ExpectExec("INSERT INTO transactions (.+) VALUES (.+)").
			WithArgs(buy.UserID, buy.ID, buy.Price).
			WillReturnError(errors.New("err"))
		m.ExpectRollback()

		r := NewRepository(sqlxDB)
		err := r.Buy(context.Background(), buy)
		assert.NotNil(t, err)
	})

	t.Run("fail to deduct user coin", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectBegin()
		m.ExpectQuery("INSERT INTO buy (.+) VALUES (.+) RETURNING id").
			WithArgs(buy.UserID, buy.ProductID, buy.Quantity, buy.Price).
			WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(buy.ID))
		m.ExpectExec("INSERT INTO transactions (.+) VALUES (.+)").
			WithArgs(buy.UserID, buy.ID, buy.Price).
			WillReturnResult(sqlmock.NewResult(1, 1))
		m.ExpectExec("UPDATE user_coin").
			WithArgs(buy.Coins[0].Quantity, buy.UserID, buy.Coins[0].Value).
			WillReturnError(errors.New("err"))
		m.ExpectRollback()

		r := NewRepository(sqlxDB)
		err := r.Buy(context.Background(), buy)
		assert.NotNil(t, err)
	})

	t.Run("fail to commit", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectBegin()
		m.ExpectQuery("INSERT INTO buy (.+) VALUES (.+) RETURNING id").
			WithArgs(buy.UserID, buy.ProductID, buy.Quantity, buy.Price).
			WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(buy.ID))
		m.ExpectExec("INSERT INTO transactions (.+) VALUES (.+)").
			WithArgs(buy.UserID, buy.ID, buy.Price).
			WillReturnResult(sqlmock.NewResult(1, 1))
		m.ExpectExec("UPDATE user_coin").
			WithArgs(buy.Coins[0].Quantity, buy.UserID, buy.Coins[0].Value).
			WillReturnResult(sqlmock.NewResult(1, 1))
		m.ExpectCommit().WillReturnError(errors.New("err"))

		r := NewRepository(sqlxDB)
		err := r.Buy(context.Background(), buy)
		assert.NotNil(t, err)
	})
}
