package product

import (
	"context"
	"fmt"
	"match-api/internal/app/model"
)

//go:generate mockgen -source service.go -destination ./mocks/mock_service.go -package mocks

type Service interface {
	CreateProduct(ctx context.Context, userType string, data model.Product) error
	GetProductList(ctx context.Context, start, limit int) ([]model.Product, error)
	BuyProduct(ctx context.Context, userCoin []model.Coin, buy model.Buy) error
}

type svc struct {
	repo repository
}

func NewService(r repository) *svc {
	return &svc{
		repo: r,
	}
}

func (s *svc) CreateProduct(ctx context.Context, userType string, data model.Product) error {
	if userType != model.USER_ROLE_SELLER {
		return fmt.Errorf("this user can't create product :%w", model.ErrInvalid)
	}
	if err := data.ValidateCreateRequest(); err != nil {
		return err
	}
	return s.repo.Insert(ctx, data)
}

func (s *svc) GetProductList(ctx context.Context, start, limit int) ([]model.Product, error) {
	return s.repo.GetProducts(ctx, start, limit)
}

func (s *svc) BuyProduct(ctx context.Context, userCoin []model.Coin, buy model.Buy) error {
	productDetails, err := s.repo.ProductDetails(ctx, buy.ProductID)
	if err != nil {
		return err
	}
	buy.Price = buy.Quantity * productDetails.Price
	coins, ok := s.coinsToBuy(userCoin, buy.Price)
	if !ok {
		return fmt.Errorf("insufficient coin :%w", model.ErrInvalid)
	}
	buy.Coins = coins
	return s.repo.Buy(ctx, buy)
}

func (s *svc) coinsToBuy(userCoin []model.Coin, amount int) ([]model.Coin, bool) {
	coins := make([]model.Coin, 0)
	for _, v := range userCoin {
		targetCoin := amount / v.Value
		if targetCoin < v.Quantity {
			v.Quantity = targetCoin
		}
		amount = amount - (v.Value * v.Quantity)
		coins = append(coins, v)
	}
	return coins, amount == 0
}
