package product

import (
	"context"
	"errors"
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"match-api/internal/app/model"
	mc "match-api/internal/app/product/mocks"
	"testing"
)

func TestSvc_CreateProduct(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	testCases := []struct {
		desc        string
		userType    string
		buyData     model.Product
		mockRepo    func() *mc.Mockrepository
		expectedErr error
	}{
		{
			desc:     "should success",
			userType: model.USER_ROLE_SELLER,
			buyData: model.Product{
				Name:    "product-1",
				Price:   10,
				OwnerId: 1,
			},
			mockRepo: func() *mc.Mockrepository {
				r := mc.NewMockrepository(ctrl)
				r.EXPECT().Insert(gomock.Any(), gomock.Any()).Return(nil)
				return r
			},
			expectedErr: nil,
		},
		{
			desc:     "invalid user role",
			userType: model.USER_ROLE_BUYER,
			buyData:  model.Product{},
			mockRepo: func() *mc.Mockrepository {
				return mc.NewMockrepository(ctrl)
			},
			expectedErr: fmt.Errorf("this user can't create product :%w", model.ErrInvalid),
		},
		{
			desc:     "invalid product",
			userType: model.USER_ROLE_SELLER,
			buyData: model.Product{
				Name:    "product-1",
				Price:   10,
				OwnerId: 1,
			},
			mockRepo: func() *mc.Mockrepository {
				r := mc.NewMockrepository(ctrl)
				r.EXPECT().Insert(gomock.Any(), gomock.Any()).Return(errors.New("db-err"))
				return r
			},
			expectedErr: errors.New("db-err"),
		},
		{
			desc:     "db error",
			userType: model.USER_ROLE_SELLER,
			buyData:  model.Product{},
			mockRepo: func() *mc.Mockrepository {
				return mc.NewMockrepository(ctrl)
			},
			expectedErr: fmt.Errorf("invalid name or price :%w", model.ErrInvalid),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			s := NewService(tc.mockRepo())
			err := s.CreateProduct(context.Background(), tc.userType, tc.buyData)
			assert.Equal(t, tc.expectedErr, err)
		})
	}
}

func TestSvc_GetProductList(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	products := []model.Product{
		{
			ID:      1,
			Name:    "product-1",
			Price:   10,
			OwnerId: 1,
		},
	}
	testCases := []struct {
		desc             string
		start            int
		limit            int
		mockRepo         func() *mc.Mockrepository
		expectedProducts []model.Product
		expectedErr      error
	}{
		{
			desc:  "should success",
			start: 0,
			limit: 10,
			mockRepo: func() *mc.Mockrepository {
				r := mc.NewMockrepository(ctrl)
				r.EXPECT().GetProducts(context.Background(), 0, 10).Return(products, nil)
				return r
			},
			expectedProducts: products,
			expectedErr:      nil,
		},
		{
			desc:  "return error",
			start: 0,
			limit: 10,
			mockRepo: func() *mc.Mockrepository {
				r := mc.NewMockrepository(ctrl)
				r.EXPECT().GetProducts(context.Background(), 0, 10).Return(nil, errors.New("err"))
				return r
			},
			expectedProducts: nil,
			expectedErr:      errors.New("err"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			s := NewService(tc.mockRepo())
			data, err := s.GetProductList(context.Background(), tc.start, tc.limit)
			assert.EqualValues(t, tc.expectedProducts, data)
			assert.Equal(t, tc.expectedErr, err)
		})
	}
}

func TestSvc_BuyProduct(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	product := model.Product{
		ID:      1,
		Name:    "product-1",
		Price:   10,
		OwnerId: 1,
	}
	testCases := []struct {
		desc        string
		coins       []model.Coin
		buyData     model.Buy
		mockRepo    func() *mc.Mockrepository
		expectedErr error
	}{
		{
			desc:  "should success",
			coins: []model.Coin{{Value: 10, Quantity: 2}},
			buyData: model.Buy{
				Quantity:  1,
				UserID:    1,
				ProductID: 1,
			},
			mockRepo: func() *mc.Mockrepository {
				r := mc.NewMockrepository(ctrl)
				r.EXPECT().ProductDetails(gomock.Any(), 1).Return(product, nil)
				r.EXPECT().Buy(gomock.Any(), gomock.Any()).Return(nil)
				return r
			},
			expectedErr: nil,
		},
		{
			desc:  "product not exist",
			coins: []model.Coin{{Value: 10, Quantity: 2}},
			buyData: model.Buy{
				Quantity:  1,
				UserID:    1,
				ProductID: 1,
			},
			mockRepo: func() *mc.Mockrepository {
				r := mc.NewMockrepository(ctrl)
				r.EXPECT().ProductDetails(gomock.Any(), 1).Return(model.Product{}, errors.New("err"))
				return r
			},
			expectedErr: errors.New("err"),
		},
		{
			desc:  "insufficient coin",
			coins: []model.Coin{{Value: 5, Quantity: 1}},
			buyData: model.Buy{
				Quantity:  1,
				UserID:    1,
				ProductID: 1,
			},
			mockRepo: func() *mc.Mockrepository {
				r := mc.NewMockrepository(ctrl)
				r.EXPECT().ProductDetails(gomock.Any(), 1).Return(product, nil)
				return r
			},
			expectedErr: fmt.Errorf("insufficient coin :%w", model.ErrInvalid),
		},
		{
			desc:  "return error",
			coins: []model.Coin{{Value: 10, Quantity: 2}},
			buyData: model.Buy{
				Quantity:  1,
				UserID:    1,
				ProductID: 1,
			},
			mockRepo: func() *mc.Mockrepository {
				r := mc.NewMockrepository(ctrl)
				r.EXPECT().ProductDetails(gomock.Any(), 1).Return(product, nil)
				r.EXPECT().Buy(gomock.Any(), gomock.Any()).Return(errors.New("err"))
				return r
			},
			expectedErr: errors.New("err"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			s := NewService(tc.mockRepo())
			err := s.BuyProduct(context.Background(), tc.coins, tc.buyData)
			assert.Equal(t, tc.expectedErr, err)
		})
	}
}
