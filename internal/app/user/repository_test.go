package user

import (
	"context"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"match-api/internal/app/model"
	"testing"
)

func TestRepo_Insert(t *testing.T) {
	t.Run("should return success", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		defer db.Close()

		user := model.User{
			Username: "a",
			Password: "1",
			UserRole: "A",
		}
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		m.ExpectPrepare("INSERT INTO users (.+) VALUES (.+)").ExpectExec().
			WithArgs(user.Username, user.Password, user.UserRole).
			WillReturnResult(sqlmock.NewResult(1, 1))

		r := NewRepository(sqlxDB)
		err := r.Insert(context.Background(), user)
		assert.Nil(t, err)
	})

	t.Run("should return query prepare error", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		defer db.Close()

		user := model.User{
			Username: "a",
			Password: "1",
			UserRole: "A",
		}
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		m.ExpectPrepare("INSERT INTO users (.+) VALUES (.+)").
			WillReturnError(errors.New("err"))

		r := NewRepository(sqlxDB)
		err := r.Insert(context.Background(), user)
		assert.NotNil(t, err)
	})

	t.Run("should return unique key violation error", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		defer db.Close()

		user := model.User{
			Username: "a",
			Password: "1",
			UserRole: "A",
		}
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		m.ExpectPrepare("INSERT INTO users (.+) VALUES (.+)").ExpectExec().
			WithArgs(user.Username, user.Password, user.UserRole).
			WillReturnError(&pq.Error{Code: "23505"})
		r := NewRepository(sqlxDB)
		err := r.Insert(context.Background(), user)
		assert.True(t, errors.Is(err, model.ErrInvalid))
	})

	t.Run("should return db error", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		defer db.Close()

		user := model.User{
			Username: "a",
			Password: "1",
			UserRole: "A",
		}
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		m.ExpectPrepare("INSERT INTO users (.+) VALUES (.+)").ExpectExec().
			WithArgs(user.Username, user.Password, user.UserRole).
			WillReturnError(errors.New("db-error"))
		r := NewRepository(sqlxDB)
		err := r.Insert(context.Background(), user)
		assert.Equal(t, errors.New("db-error"), err)
	})
}

func TestRepo_MatchUsernameAndPassword(t *testing.T) {
	t.Run("should success", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		user := model.User{
			ID:       1,
			Username: "asd",
			Password: "123",
			UserRole: "SELLER",
		}
		m.ExpectQuery("SELECT id, username, password, user_role FROM users ").
			WithArgs(user.Username, user.Password).
			WillReturnRows(sqlmock.NewRows([]string{"id", "username", "password", "user_role"}).AddRow(user.ID, user.Username, user.Password, user.UserRole))

		r := NewRepository(sqlxDB)
		data, err := r.MatchUsernameAndPassword(context.Background(), user.Username, user.Password)
		assert.EqualValues(t, user, data)
		assert.Nil(t, err)
	})

	t.Run("should return error", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectQuery("SELECT id, username, password, user_role FROM users ").
			WithArgs("asd", "123").
			WillReturnError(errors.New("db-err"))

		r := NewRepository(sqlxDB)
		data, err := r.MatchUsernameAndPassword(context.Background(), "asd", "123")
		assert.EqualValues(t, model.User{}, data)
		assert.NotNil(t, err)
	})
}

func TestRepo_SaveToken(t *testing.T) {
	t.Run("should success", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectExec("INSERT INTO auth_token").
			WithArgs(1, "token").
			WillReturnResult(sqlmock.NewResult(1, 1))

		r := NewRepository(sqlxDB)
		err := r.SaveToken(context.Background(), 1, "token")
		assert.Nil(t, err)
	})

	t.Run("should return error", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectExec("INSERT INTO auth_token (user_id, token) VALUES (.+)").
			WithArgs(1, "token").
			WillReturnError(errors.New("db-err"))

		r := NewRepository(sqlxDB)
		err := r.SaveToken(context.Background(), 1, "token")
		assert.NotNil(t, err)
	})
}

func TestRepo_GetUserByToken(t *testing.T) {
	t.Run("should success", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		user := model.User{
			ID:       1,
			Username: "asd",
			Password: "123",
			UserRole: "SELLER",
		}

		m.ExpectQuery("SELECT users.id, users.username, users.password, users.user_role FROM users").
			WithArgs("token").
			WillReturnRows(sqlmock.NewRows([]string{"id", "username", "password", "user_role"}).AddRow(user.ID, user.Username, user.Password, user.UserRole))

		r := NewRepository(sqlxDB)
		data, err := r.GetUserByToken(context.Background(), "token")
		assert.EqualValues(t, user, data)
		assert.Nil(t, err)
	})

	t.Run("should return error", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectQuery("SELECT users.id, users.username, users.password, users.user_role FROM users").
			WithArgs("token").
			WillReturnError(errors.New("db-err"))

		r := NewRepository(sqlxDB)
		data, err := r.GetUserByToken(context.Background(), "token")
		assert.EqualValues(t, model.User{}, data)
		assert.NotNil(t, err)
	})
}

func TestRepo_UserCoins(t *testing.T) {
	t.Run("should success", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		coin := []model.Coin{{
			Value:    10,
			Quantity: 2,
		}}

		m.ExpectQuery("ELECT coin, quantity FROM user_coin").
			WithArgs(1).
			WillReturnRows(sqlmock.NewRows([]string{"coin", "quantity"}).AddRow(coin[0].Value, coin[0].Quantity))

		r := NewRepository(sqlxDB)
		data, err := r.UserCoins(context.Background(), 1)
		assert.EqualValues(t, coin, data)
		assert.Nil(t, err)
	})

	t.Run("should return error", func(t *testing.T) {
		db, m, _ := sqlmock.New()
		sqlxDB := sqlx.NewDb(db, "sqlmock")
		defer db.Close()

		m.ExpectQuery("ELECT coin, quantity FROM user_coin").
			WithArgs(1).
			WillReturnError(errors.New("dn-err"))

		r := NewRepository(sqlxDB)
		data, err := r.UserCoins(context.Background(), 1)
		assert.Nil(t, data)
		assert.NotNil(t, err)
	})
}
