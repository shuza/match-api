package user

import (
	context "context"
	"errors"
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"match-api/internal/app/model"
	mc "match-api/internal/app/user/mocks"
	"testing"
)

func TestSvc_CreateUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	testCases := []struct {
		desc        string
		data        model.User
		mockRepo    func() *mc.Mockrepository
		expectedErr error
	}{
		{
			desc: "should return success",
			data: model.User{
				Username: "a",
				Password: "a",
				UserRole: "SELLER",
			},
			mockRepo: func() *mc.Mockrepository {
				r := mc.NewMockrepository(ctrl)
				r.EXPECT().Insert(gomock.Any(), gomock.Any()).Return(nil)
				return r
			},
			expectedErr: nil,
		},
		{
			desc: "should return validation error",
			data: model.User{},
			mockRepo: func() *mc.Mockrepository {
				return mc.NewMockrepository(ctrl)
			},
			expectedErr: fmt.Errorf("invliad username or password :%w", model.ErrInvalid),
		},
		{
			desc: "should return repository error",
			data: model.User{
				Username: "a",
				Password: "a",
				UserRole: "SELLER",
			},
			mockRepo: func() *mc.Mockrepository {
				r := mc.NewMockrepository(ctrl)
				r.EXPECT().Insert(gomock.Any(), gomock.Any()).Return(errors.New("repo-error"))
				return r
			},
			expectedErr: errors.New("repo-error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			s := NewService(tc.mockRepo())
			err := s.CreateUser(context.Background(), tc.data)
			assert.Equal(t, tc.expectedErr, err)
		})
	}
}

func TestSvc_MatchUsernameAndPassword(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	user := model.User{
		ID:       1,
		Username: "asd",
		Password: "123",
		UserRole: "SELLER",
	}
	testCases := []struct {
		desc         string
		username     string
		password     string
		mockRepo     func() *mc.Mockrepository
		expectedErr  error
		expectedUser model.User
	}{
		{
			desc:     "should success",
			username: user.Username,
			password: user.Password,
			mockRepo: func() *mc.Mockrepository {
				r := mc.NewMockrepository(ctrl)
				r.EXPECT().MatchUsernameAndPassword(gomock.Any(), user.Username, user.Password).Return(user, nil)
				return r
			},
			expectedErr:  nil,
			expectedUser: user,
		},
		{
			desc:     "invalid error",
			username: "",
			password: "",
			mockRepo: func() *mc.Mockrepository {
				return mc.NewMockrepository(ctrl)
			},
			expectedErr:  fmt.Errorf("request username and password :%w", model.ErrInvalid),
			expectedUser: model.User{},
		},
		{
			desc:     "return db error",
			username: user.Username,
			password: user.Password,
			mockRepo: func() *mc.Mockrepository {
				r := mc.NewMockrepository(ctrl)
				r.EXPECT().MatchUsernameAndPassword(gomock.Any(), user.Username, user.Password).Return(model.User{}, errors.New("db-err"))
				return r
			},
			expectedErr:  errors.New("db-err"),
			expectedUser: model.User{},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			s := NewService(tc.mockRepo())
			data, err := s.MatchUsernameAndPassword(context.Background(), tc.username, tc.password)
			assert.EqualValues(t, tc.expectedUser, data)
			assert.Equal(t, tc.expectedErr, err)
		})
	}
}

func TestSvc_CreateAuthToken(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	testCases := []struct {
		desc        string
		userID      int
		mockRepo    func() *mc.Mockrepository
		expectedErr error
		emptyToken  bool
	}{
		{
			desc:   "should success",
			userID: 1,
			mockRepo: func() *mc.Mockrepository {
				r := mc.NewMockrepository(ctrl)
				r.EXPECT().SaveToken(gomock.Any(), 1, gomock.Any()).Return(nil)
				return r
			},
			expectedErr: nil,
			emptyToken:  false,
		},
		{
			desc:   "should success",
			userID: 1,
			mockRepo: func() *mc.Mockrepository {
				r := mc.NewMockrepository(ctrl)
				r.EXPECT().SaveToken(gomock.Any(), 1, gomock.Any()).Return(errors.New("err"))
				return r
			},
			expectedErr: errors.New("err"),
			emptyToken:  true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			s := NewService(tc.mockRepo())
			token, err := s.CreateAuthToken(context.Background(), 1)
			assert.Equal(t, tc.expectedErr, err)
			assert.Equal(t, tc.emptyToken, token == "")
		})
	}
}

func TestSvc_GetUserByToken(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	user := model.User{
		ID:       1,
		Username: "asd",
		Password: "123",
		UserRole: "SELLER",
	}
	testCases := []struct {
		desc         string
		token        string
		mockRepo     func() *mc.Mockrepository
		expectedErr  error
		expectedUser model.User
	}{
		{
			desc:  "should success",
			token: "token",
			mockRepo: func() *mc.Mockrepository {
				r := mc.NewMockrepository(ctrl)
				r.EXPECT().GetUserByToken(gomock.Any(), "token").Return(user, nil)
				return r
			},
			expectedErr:  nil,
			expectedUser: user,
		},
		{
			desc:  "return error",
			token: "token",
			mockRepo: func() *mc.Mockrepository {
				r := mc.NewMockrepository(ctrl)
				r.EXPECT().GetUserByToken(gomock.Any(), "token").Return(model.User{}, errors.New("err"))
				return r
			},
			expectedErr:  errors.New("err"),
			expectedUser: model.User{},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			s := NewService(tc.mockRepo())
			data, err := s.GetUserByToken(context.Background(), tc.token)
			assert.EqualValues(t, tc.expectedUser, data)
			assert.Equal(t, tc.expectedErr, err)
		})
	}
}

func TestSvc_GetUserCoin(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	coins := []model.Coin{{Value: 10, Quantity: 1}}
	testCases := []struct {
		desc          string
		userID        int
		mockRepo      func() *mc.Mockrepository
		expectedErr   error
		expectedCoins []model.Coin
	}{
		{
			desc:   "should success",
			userID: 1,
			mockRepo: func() *mc.Mockrepository {
				r := mc.NewMockrepository(ctrl)
				r.EXPECT().UserCoins(gomock.Any(), 1).Return(coins, nil)
				return r
			},
			expectedErr:   nil,
			expectedCoins: coins,
		},
		{
			desc:   "return error",
			userID: 1,
			mockRepo: func() *mc.Mockrepository {
				r := mc.NewMockrepository(ctrl)
				r.EXPECT().UserCoins(gomock.Any(), 1).Return(nil, errors.New("err"))
				return r
			},
			expectedErr:   errors.New("err"),
			expectedCoins: nil,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			s := NewService(tc.mockRepo())
			data, err := s.GetUserCoin(context.Background(), tc.userID)
			assert.EqualValues(t, tc.expectedCoins, data)
			assert.Equal(t, tc.expectedErr, err)
		})
	}
}
