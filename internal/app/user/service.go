package user

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"match-api/internal/app/model"
)

//go:generate mockgen -source service.go -destination ./mocks/mock_service.go -package mocks

type Service interface {
	CreateUser(ctx context.Context, data model.User) error
	MatchUsernameAndPassword(ctx context.Context, username, password string) (model.User, error)
	CreateAuthToken(ctx context.Context, userID int) (string, error)
	GetUserByToken(ctx context.Context, token string) (model.User, error)
	GetUserCoin(ctx context.Context, userID int) ([]model.Coin, error)
}

type svc struct {
	repo repository
}

func NewService(r repository) *svc {
	return &svc{
		repo: r,
	}
}

func (s *svc) CreateUser(ctx context.Context, data model.User) error {
	if err := data.ValidateCreateRequest(); err != nil {
		return err
	}
	return s.repo.Insert(ctx, data)
}

func (s *svc) MatchUsernameAndPassword(ctx context.Context, username, password string) (model.User, error) {
	if username == "" || password == "" {
		return model.User{}, fmt.Errorf("request username and password :%w", model.ErrInvalid)
	}
	return s.repo.MatchUsernameAndPassword(ctx, username, password)
}

func (s *svc) CreateAuthToken(ctx context.Context, userID int) (string, error) {
	token := uuid.NewString()
	if err := s.repo.SaveToken(ctx, userID, token); err != nil {
		return "", err
	}
	return token, nil
}

func (s *svc) GetUserByToken(ctx context.Context, token string) (model.User, error) {
	return s.repo.GetUserByToken(ctx, token)
}

func (s *svc) GetUserCoin(ctx context.Context, userID int) ([]model.Coin, error) {
	return s.repo.UserCoins(ctx, userID)
}
