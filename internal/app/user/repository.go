package user

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	"github.com/rs/zerolog/log"
	"match-api/internal/app/model"
)

//go:generate mockgen -source repository.go -destination ./mocks/mock_repository.go -package mocks

type repository interface {
	Insert(ctx context.Context, user model.User) error
	MatchUsernameAndPassword(ctx context.Context, username, password string) (model.User, error)
	SaveToken(ctx context.Context, userID int, token string) error
	GetUserByToken(ctx context.Context, token string) (model.User, error)
	UserCoins(ctx context.Context, userID int) ([]model.Coin, error)
}

const (
	errUniqueViolation            = pq.ErrorCode("23505")
	InsertUserQuery               = "INSERT INTO users (username, password, user_role) VALUES (:username, :password, :user_role)"
	MatchUsernameAndPasswordQuery = "SELECT id, username, password, user_role FROM users WHERE username = $1 AND password = $2"
	saveTokenQuery                = "INSERT INTO auth_token (user_id, token) VALUES ($1, $2)"
	userByTokenQuery              = "SELECT users.id, users.username, users.password, users.user_role FROM users INNER JOIN auth_token ON users.id = auth_token.user_id WHERE auth_token.token = $1"
	userCoinQuery                 = "SELECT coin, quantity FROM user_coin WHERE user_id = $1"
)

type repo struct {
	db *sqlx.DB
}

func NewRepository(db *sqlx.DB) *repo {
	return &repo{
		db: db,
	}
}

func (r *repo) Insert(ctx context.Context, user model.User) error {
	stmt, err := r.db.PrepareNamedContext(ctx, InsertUserQuery)
	if err != nil {
		log.Error().Err(err).Msg("[user.Insert] failed to insert user data")
		return err
	}
	_, err = stmt.ExecContext(ctx, &user)
	if err != nil {
		if pqErr, ok := err.(*pq.Error); ok && pqErr.Code == errUniqueViolation {
			return fmt.Errorf("%v :%w", err, model.ErrInvalid)
		}
		log.Error().Err(err).Msg("[user.Insert] failed to execute query")
		return err
	}
	return nil
}

func (r *repo) MatchUsernameAndPassword(ctx context.Context, username, password string) (model.User, error) {
	var data model.User
	if err := r.db.GetContext(ctx, &data, MatchUsernameAndPasswordQuery, username, password); err != nil {
		return model.User{}, err
	}
	return data, nil
}

func (r *repo) SaveToken(ctx context.Context, userID int, token string) error {
	if _, err := r.db.ExecContext(ctx, saveTokenQuery, userID, token); err != nil {
		log.Error().Err(err).Msg("[SaveToken] failed to save auth_token")
		return err
	}
	return nil
}

func (r *repo) GetUserByToken(ctx context.Context, token string) (model.User, error) {
	var user model.User
	if err := r.db.GetContext(ctx, &user, userByTokenQuery, token); err != nil {
		log.Error().Err(err).Msgf("[GetUserByToken] failed to fetch user for token: %s", token)
		return model.User{}, err
	}
	return user, nil
}

func (r *repo) UserCoins(ctx context.Context, userID int) ([]model.Coin, error) {
	var coins []model.Coin
	if err := r.db.SelectContext(ctx, &coins, userCoinQuery, userID); err != nil {
		log.Error().Err(err).Msgf("[user.UserCoins] failed to fetch coin for user_id: %d", userID)
		return nil, err
	}
	return coins, nil
}
