# Configuration
update database credentials in `application.yml` file. To create sample configuration file execute
```shell
make copy-config
```

# Test
To run test execute. Current minimum coverage 80% we can increase the coverage but need more time. 
```shell
make test
```

# Migration
To run migration script execute
```shell
make migrate
```

# Rollback
By default it will rolback 1 step. To rollback execute
```shell
make rollback
```

# Server
To start api server execute
```shell
make server
```

# Doc
You can find postman api collection in `doc` directory. The `Login` api request wll add auth token to env and all other api are using this env token so you don't have store it manually.