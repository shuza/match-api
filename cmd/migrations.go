package cmd

import (
	"github.com/spf13/viper"
	postgres2 "match-api/internal/pkg/postgres"

	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/spf13/cobra"
)

var migrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "Start DB migrations",
	Long:  `It will run migrations according to db/migrations scripts on Reporting database`,
	RunE: func(cmd *cobra.Command, args []string) error {
		return postgres2.RunDatabaseMigration(&postgres2.Config{
			Host:     viper.GetString("DB_HOST"),
			Port:     viper.GetString("DB_PORT"),
			User:     viper.GetString("DB_USER"),
			Password: viper.GetString("DB_PASSWORD"),
			Name:     viper.GetString("DB_NAME"),
		})
	},
}

var rollbackCmd = &cobra.Command{
	Use:   "rollback",
	Short: "Start DB rollback",
	Long:  "It will rollback one step",
	RunE: func(cmd *cobra.Command, args []string) error {
		return postgres2.RollbackLatestMigration(&postgres2.Config{
			Host:     viper.GetString("DB_HOST"),
			Port:     viper.GetString("DB_PORT"),
			User:     viper.GetString("DB_USER"),
			Password: viper.GetString("DB_PASSWORD"),
			Name:     viper.GetString("DB_NAME"),
		})
	},
}

func init() {
	rootCmd.AddCommand(migrateCmd)
	rootCmd.AddCommand(rollbackCmd)
}
