package cmd

import (
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"match-api/internal/app/deposit"
	"match-api/internal/app/product"
	"match-api/internal/app/server"
	"match-api/internal/app/user"
	"match-api/internal/pkg/postgres"
	"os"
	"os/signal"
	"syscall"
)

var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "Start server",
	Long:  `Start server`,
	RunE: func(cmd *cobra.Command, args []string) error {
		db, err := postgres.New(&postgres.Config{
			Host:     viper.GetString("DB_HOST"),
			Port:     viper.GetString("DB_PORT"),
			User:     viper.GetString("DB_USER"),
			Password: viper.GetString("DB_PASSWORD"),
			Name:     viper.GetString("DB_NAME"),
		})
		if err != nil {
			log.Error().Err(err).Msg("failed to connect DB")
			return err
		}
		defer db.Close()

		s := server.NewServer(viper.GetString("APP_PORT"),
			user.NewService(user.NewRepository(db)),
			product.NewService(product.NewRepository(db)),
			deposit.NewService(deposit.NewRepository(db)),
		)
		sig := make(chan os.Signal, 1)
		signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)

		go func() {
			<-sig
			if err := s.Shutdown(); err != nil {
				log.Error().Err(err).Msg("error during server shutdown")
			}
		}()

		return s.Run()
	},
}

func init() {
	rootCmd.AddCommand(serverCmd)
}
