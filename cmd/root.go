package cmd

import (
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"strings"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "match-server",
	Short: "match-server",
	Long:  `match-server`,
}

func init() {
	viper.SetConfigName("application")
	viper.AddConfigPath(".")
	viper.SetConfigType("yaml")
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_", ".", "_"))
	viper.AutomaticEnv()
	if err := viper.ReadInConfig(); err != nil {
		log.Info().Err(err).Str("config_file", viper.ConfigFileUsed()).Msg("failed reading from config file")
	}

	log.Info().Interface("configs", viper.AllSettings()).Msg("config initialized")
}

func Execute() error {
	return rootCmd.Execute()
}
