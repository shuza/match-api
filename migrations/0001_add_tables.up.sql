CREATE TABLE IF NOT EXISTS users (
  id SERIAL PRIMARY KEY,
  username TEXT NOT NULL UNIQUE,
  password TEXT NOT NULL,
  user_role TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS auth_token (
    id SERIAL PRIMARY KEY,
    user_id INT REFERENCES users (id),
    token TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS user_coin (
    user_id INT REFERENCES users (id),
    coin INT NOT NULL,
    quantity INT NOT NULL,
    PRIMARY KEY (user_id, coin)
);

CREATE TABLE IF NOT EXISTS products (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL UNIQUE,
    price FLOAT NOT NULL,
    owner_id INT REFERENCES users (id)
);

CREATE TABLE IF NOT EXISTS deposit (
    id SERIAL PRIMARY KEY,
    user_id INT REFERENCES users (id),
    coin INT NOT NULL,
    quantity INT NOT NULL,
    amount INT NOT NULL
);

CREATE TABLE IF NOT EXISTS buy (
    id SERIAL PRIMARY KEY,
    user_id INT REFERENCES users (id),
    product_id INT REFERENCES products (id),
    quantity INT NOT NULL,
    price INT NOT NULL
);

CREATE TABLE IF NOT EXISTS transactions (
    id SERIAL PRIMARY KEY,
    user_id INT REFERENCES users (id),
    buy_id INT REFERENCES buy (id),
    amount FLOAT NOT NULL
);